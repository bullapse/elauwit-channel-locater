package com.bullapse.elauwitchannelfinder;

import java.util.ArrayList;

/**
 * Created by Spencer Bull on 9/13/2014.
 */
public class ChannelListings {

    //class variables
    ArrayList<Channel> channels = new ArrayList<Channel>();

    //ChannelListings constructor
    public ChannelListings() {

        setChannels();


    }

    public void setChannels() {
        channels.add(new Channel(2, "Channel Guide"));
        channels.add(new Channel(3, "Local Property Channel"));
        channels.add(new Channel(4, "CBS"));
        channels.add(new Channel(5, "ABC"));
        channels.add(new Channel(6, "NBC"));
        channels.add(new Channel(7, "FOX"));
        channels.add(new Channel(8, "PBS"));
        channels.add(new Channel(9, "CW"));
        channels.add(new Channel(10, "Bloomberg"));
        channels.add(new Channel(11, "CNBC"));
        channels.add(new Channel(12, "MSNBC"));
        channels.add(new Channel(13, "CNN"));
        channels.add(new Channel(14, "CNN Headline News"));
        channels.add(new Channel(15, "FOX News"));
        channels.add(new Channel(16, "CSPAN"));
        channels.add(new Channel(17, "CSPAN 2"));
        channels.add(new Channel(18, "BBC America"));
        channels.add(new Channel(19, "TNT"));
        channels.add(new Channel(20, "USA"));
        channels.add(new Channel(21, "SyFy"));
        channels.add(new Channel(22, "TBS"));
        channels.add(new Channel(23, "FLIX"));
        channels.add(new Channel(24, "ESPN"));
        channels.add(new Channel(25, "ESPN2"));
        channels.add(new Channel(26, "ESPN News"));
        channels.add(new Channel(27, "ESPN Classic"));
        channels.add(new Channel(28, "ESPNU"));
        channels.add(new Channel(29, "Animal Planet"));
        channels.add(new Channel(30, "NatGeo"));
        channels.add(new Channel(31, "Discovery"));
        channels.add(new Channel(32, "Science Channel"));
        channels.add(new Channel(33, "DIY"));
        channels.add(new Channel(34, "Food Network"));
        channels.add(new Channel(35, "History"));
        channels.add(new Channel(36, "Travel"));
        channels.add(new Channel(37, "HGTV"));
        channels.add(new Channel(38, "TLC"));
        channels.add(new Channel(39, "The Weather Channel"));
        channels.add(new Channel(40, "A&E"));
        channels.add(new Channel(41, "AMC"));
        channels.add(new Channel(42, "TruTV"));
        channels.add(new Channel(43, "TBN"));
        channels.add(new Channel(44, "TV Land"));
        channels.add(new Channel(45, "Comedy Central"));
        channels.add(new Channel(46, "Spike Television"));
        channels.add(new Channel(47, "E!"));
        channels.add(new Channel(48, "MTV"));
        channels.add(new Channel(49, "MTV2"));
        channels.add(new Channel(50, "VH1"));
        channels.add(new Channel(51, "VH1 Classic"));
        channels.add(new Channel(52, "CMT"));
        channels.add(new Channel(53, "BET"));
        channels.add(new Channel(54, "Cartoon Network"));
        channels.add(new Channel(55, "Nickelodeon"));
        channels.add(new Channel(56, "ABC Family Channel"));
        channels.add(new Channel(57, "CBS Sports"));
        channels.add(new Channel(58, "NFL Network"));
        channels.add(new Channel(59, "The Golf Channel"));
        channels.add(new Channel(60, "MLB Network"));
        channels.add(new Channel(61, "NBC Sports"));
        channels.add(new Channel(62, "Longhorn Network"));
        channels.add(new Channel(63, "Fox Sports One"));
        channels.add(new Channel(64, "Hallmark"));
        channels.add(new Channel(65, "OWN"));
        channels.add(new Channel(66, "Bravo"));
        channels.add(new Channel(67, "Lifetime"));
        channels.add(new Channel(68, "Oxygen"));
        channels.add(new Channel(69, "Biography"));
        channels.add(new Channel(70, "Outdoor Channel"));
        channels.add(new Channel(71, "Cooking Channel"));
        channels.add(new Channel(72, "Great American Country"));
        channels.add(new Channel(73, "Lifetime Movie Network"));
        channels.add(new Channel(74, "G4"));
        channels.add(new Channel(75, "Showcase"));
        channels.add(new Channel(76, "ShowTime"));
        channels.add(new Channel(77, "ShowTime Beyond"));
        channels.add(new Channel(78, "ShowTime Extreme"));
        channels.add(new Channel(79, "ShowTime Too "));
    }

    public ArrayList<Channel> getChannelListings() {
        return channels;
    }
}
