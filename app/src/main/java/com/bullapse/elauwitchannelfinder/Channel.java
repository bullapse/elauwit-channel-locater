package com.bullapse.elauwitchannelfinder;

/**
 * Created by Spencer Bull on 9/13/2014.
 *
 * Simple Object that Stores the values of a number and a name of a specific number
 */
public class Channel {

    //class variables
    int number;
    String name;
    //constructor
    public Channel(int num, String n) {
        number = num;
        name = n;
    }

    //return the channel number
    public int getNumber() {
        return number;
    }

    //return the channel name
    public String getName() {
        return name;
    }
}
