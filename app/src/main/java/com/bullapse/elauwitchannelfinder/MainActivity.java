package com.bullapse.elauwitchannelfinder;

import com.google.android.glass.app.Card;
import com.google.android.glass.media.Sounds;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link Activity} showing a tuggable "Hello World!" card.
 * <p>
 * The main content view is composed of a one-card {@link CardScrollView} that provides tugging
 * feedback to the user when swipe gestures are detected.
 * If your Glassware intends to intercept swipe gestures, you should set the content view directly
 * and use a {@link com.google.android.glass.touchpad.GestureDetector}.
 * @see <a href="https://developers.google.com/glass/develop/gdk/touch">GDK Developer Guide</a>
 */
public class MainActivity extends Activity {

    /** static **/
    private static final int SPEECH_REQUEST = 0;

    /** {@link CardScrollView} to use as the main content view. */
    private CardScrollView mCardScroller;
    private ChannelListings CL;
    private ArrayList<Channel> channelListings;
    private String textResult;
    private View mView;
    TextView number;
    TextView name;



    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        //sets the value of the channels
        CL = new ChannelListings();
        channelListings = CL.getChannelListings();

        setContentView(R.layout.activity_show_channel_number);
        number = (TextView) findViewById(R.id.number);
        name = (TextView) findViewById(R.id.name);

        displaySpeechRecognizer();




//        mCardScroller = new CardScrollView(this);
//        mCardScroller.setAdapter(new CardScrollAdapter() {
//            @Override
//            public int getCount() {
//                return 1;
//            }
//
//            @Override
//            public Object getItem(int position) {
//                return mView;
//            }
//
//            @Override
//            public View getView(int position, View convertView, ViewGroup parent) {
//                return mView;
//            }
//
//            @Override
//            public int getPosition(Object item) {
//                if (mView.equals(item)) {
//                    return 0;
//                }
//                return AdapterView.INVALID_POSITION;
//            }
//        });
//        // Handle the TAP event.
//        mCardScroller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                // Plays disallowed sound to indicate that TAP actions are not supported.
//                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//                am.playSoundEffect(Sounds.DISALLOWED);
//            }
//        });
        setContentView(mCardScroller);
    }

    /**Speach Recognizer*/
    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        startActivityForResult(intent, SPEECH_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == SPEECH_REQUEST && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            // call the method to look for the match.
            textResult = findMatch(spokenText);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
//
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mCardScroller.activate();
//    }
//
//    @Override
//    protected void onPause() {
//        mCardScroller.deactivate();
//        super.onPause();
//    }
//
//    /**
//     * Builds a Glass styled "Hello World!" view using the {@link Card} class.
//     */
//    private View buildView() {
//        Card card = new Card(this);
//
//        card.setText(R.string.hello_world);
//        return card.getView();
//    }

    public String findMatch(String text) {
        for (int i = 0; i < channelListings.size(); i++) {
            if (channelListings.get(i).getName().equals(text)) {
                name.setText(channelListings.get(i).getName());
                number.setText(channelListings.get(i).getNumber());
                return text;
            }
        }
        number.setText("Match Not Found");
        return "Match Not Found";
    }


}
